#!/usr/bin/env bash

# set overall stuff.
NOW=$(date +"%mm%dd%Hh%Mm%Ss")
unique=$(uuid)
git config --global gitreview.username "mynameismevin"
git config --global user.name "Mike Lloyd"
git config --global user.email "kevin.michael.lloyd@gmail.com"
gpg --keyserver keyserver.ubuntu.com --recv-key 8EC052C0
gpg --export --armor 8EC052C0 | apt-key add -

# testing upload functionality.
andromeda init -u https://github.com/mynameismevin/andromeda-sandbox
andromeda sync -j8
andromeda start "$unique" sandbox
cd sandbox
touch andromeda-"$NOW".test
echo "$unique" > andromeda-"$NOW".test
git add .
git commit -m "Running test $unique for Travis build."
cd ..
andromeda upload
rm -rf .andromeda/

# testing branch abandonment.
andromeda init -u https://github.com/mynameismevin/andromeda-sandbox
andromeda sync -j8
andromeda start "$unique" sandbox
andromeda abandon "$unique" sandbox