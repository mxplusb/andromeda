Master Status:

[![Build Status](https://travis-ci.org/mynameismevin/andromeda.svg?branch=master)](https://travis-ci.org/mynameismevin/andromeda)
[![Documentation Status](https://readthedocs.org/projects/andromeda/badge/?version=latest)](https://readthedocs.org/projects/andromeda/?badge=latest)

# Andromeda
Repository management for OpenStack. This is a shameless refactor of the [repo](https://android.googlesource.com/tools/repo/) project make for Android. While it is mostly identical for most intents and purposes, there are some differences:

- [ ] Works in Python 3 "better". Still mildly buggy.
- [ ] PEP 8 compliant.
- [x] It is configured for the OpenStack Gerrit instance, not Google's.
- [x] Andromeda does not assume Gerrit user name is the first part of your email address...it must be set in the git global configuration.
- [x] This is an actual package instead of running `curl http://... > file`.

## Testing/Quickstart with the OpenStack Sandbox
To understand some concepts, let's walk through a quick demo with the OpenStack Sandbox repository.

1. `git config --global gitreview.username yourgerritusername`
2. `andromeda init -u https://github.com/mynameismevin/andromeda-sandbox --config-name`
3. `andromeda sync -j8`
4. `andromeda start test-branch-01 sandbox`
5. `cd sandbox && touch "test-file-01" && echo "Andromeda is awesome!" > test-file-01 && git add . && git commit -m "Andromeda still rocks!" && cd .."`
6. `andromeda upload`
7. Profit.

## Advanced Usage
Please see the [docs](http://andromeda.readthedocs.org)!
