.. _commands:
.. highlight:: python

Commands
========
Here is a list of the current commands that Andromeda supports:
``abandon, branch, branches, checkout, diff, download, grep, init, prune, rebase, smartsync, stage, start, status, sync, upload``

Abandon
-------

``andromeda abandon`` permanently abandons a development branch by deleting it (and all its history) from your local repository.

It is equivalent to ``git branch -D <branchname>``.

Branch/Branches
---------------

Summarizes the currently available topic branches.

Branch Display
^^^^^^^^^^^^^^

The branch display output by this command is organized into four
columns of information; for example::

    *P nocolor                   | in repo
       repo2                     |

The first column contains a * if the branch is the currently checked out branch in any of the specified projects, or a blank if no project has the branch checked out.

The second column contains either blank, p or P, depending upon the upload status of the branch::

    (blank): branch not yet published by andromeda upload

    P: all commits were published by andromeda upload

    p: only some commits were published by andromeda upload

The third column contains the branch name.

The fourth column (after the | separator) lists the projects that the branch appears in, or does not appear in.  If no project list is shown, then the branch appears in all projects.

Checkout
--------

``andromeda checkout`` checks out an existing branch that was previously
created by 'andromeda start'.

The command is equivalent to:

	``andromeda forall [<project>...] -c git checkout <branchname>``

Diff
----

``andromeda diff`` shows the differences between the latest commit and your working tree.

Download
--------

``andromeda download`` downloads a change from the OpenStack Review system and makes it available in your project's local working directory.

Forall
------

Executes the same shell command in each project.

Output Formatting
^^^^^^^^^^^^^^^^^

The ``-p`` option causes Andromeda to bind pipes to the command's stdin, stdout and stderr streams, and pipe all output into a continuous stream that is displayed in a single pager session.  Project headings are inserted before the output of each command is displayed.  If the command produces no output in a project, no heading is displayed.

The formatting convention used by ``-p`` is very suitable for some types of searching, e.g. ``andromeda forall -p -c git log -SFoo`` will print all commits that add or remove references to ``Foo``.

The ``-v`` option causes Andromeda to display stderr messages if a command produces output only on stderr.  Normally the ``-p`` option causes command output to be suppressed until the command produces at least one byte of output on stdout.

Environment
^^^^^^^^^^^

The ``pwd`` variable is the project's working directory.  If the current client is a mirror client, then ``pwd`` is the Git repository.

``ANDROMEDA_PROJECT`` is set to the unique name of the project.

``ANDROMEDA_PATH`` is the path relative the the root of the client.

``ANDROMEDA_REMOTE`` is the name of the remote system from the manifest.

``ANDROMEDA_LREV`` is the name of the revision from the manifest, translated to a local tracking branch.  If you need to pass the manifest revision to a locally executed git command, use ``ANDROMEDA_LREV``.

``ANDROMEDA_RREV`` is the name of the revision from the manifest, exactly as written in the manifest.

Shell positional arguments (``$1 $2 .. $#``) are set to any arguments following ``<command>``. Unless ``-p`` is used, stdin, stdout, stderr are inherited from the terminal and are not redirected.

Grep
----

Search for the specified patterns in all project files.

Boolean Options
^^^^^^^^^^^^^^^

The following options can appear as often as necessary to express the pattern to locate::

    -e PATTERN
    --and, --or, --not, -(, -)

Further, the ``-r/--revision`` option may be specified multiple times in order to scan multiple trees.  If the same file matches in more than one tree, only the first result is reported, prefixed by the revision name it was found under.

Examples
^^^^^^^^

Look for a line that has ``'#define'`` and either ``'MAX_PATH`` or ``'PATH_MAX'``::

    andromeda grep -e '#define' --and -\\( -e MAX_PATH -e PATH_MAX \\)

Look for a line that has ``'NODE'`` or ``'Unexpected'`` in files that contain a line that matches both expressions::

    andromeda grep --all-match -e NODE -e Unexpected


Init
-------
Initialize andromeda in the current directory. The ``andromeda init`` command is run once to install and initialize
andromeda. The latest andromeda source code and manifest collection is downloaded from the server and is installed in the .andromeda/ directory.

The optional ``-b`` argument can be used to select the manifest branch to checkout and use. If no branch is specified, master is assumed.

The optional ``-m`` argument can be used to specify an alternate manifest to be used. If no manifest is specified, the manifest default.xml will be used.

The ``--reference`` option can be used to point to a directory that has the content of a ``--mirror sync``. This will make the working directory use as much data as possible from the local reference directory when fetching from the server. This will make the sync go a lot faster by reducing data traffic on the network.

Switching Manifest Branches
^^^^^^^^^^^^^^^^^^^^^^^^^^^
To switch to another manifest branch, ``andromeda init -b otherbranch`` may be used in an existing client. However, as this only updates the manifest, a subsequent ``andromeda sync`` (or ``andromeda sync -d``) is necessary to update the working directory files.

Prune
-----
Prune (delete) already merged topics.

Rebase
------
Rebase local branches on upstream branch.

``andromeda rebase`` uses git rebase to move local changes in the current topic branch to the HEAD of the upstream history, useful when you have made commits in a topic branch but need to incorporate new upstream changes "underneath" them.

Smartsync
---------
``andromeda smartsync`` is a shortcut for sync -s.

Stage
-----
``andromeda stage`` stages files to prepare the next commit.

Start
-----
Start a new branch for development. ``andromeda start`` begins a new branch of development, starting from the revision specified in the manifest.

Status
------
Show the working tree status. ``andromeda status`` compares the working tree to the staging area (aka the index), and the most recent commit on this branch (HEAD), in each project specified. A summary is displayed, one line per file where there is a difference between these three states.

The ``-j``/``--jobs`` option can be used to run multiple status queries in parallel.

The ``-o``/``--orphans`` option can be used to show objects that are in the working directory, but not associated with an andromeda project. This includes unmanaged top-level files and directories, but also includes deepe items. For example, if ``dir/subdir/proj1`` and ``dir/subdir/proj2`` are andromeda projects, ``dir/subdir/proj3`` will be shown if it is not known to andromeda.

Status Display
--------------
The status display is organized into three columns of information, for example if the file 'subcmds/status.py' is modified in the project 'devstack' on branch 'devwork'::

    project devstack/                                   branch devwork
    -m     subcmds/status.py

The first column explains how the staging area (index) differs from the last commit (HEAD). Its values are always displayed in upper case and have the following meanings::

    -:  no difference
    A:  added         (not in HEAD,     in index                     )
    M:  modified      (    in HEAD,     in index, different content  )
    D:  deleted       (    in HEAD, not in index                     )
    R:  renamed       (not in HEAD,     in index, path changed       )
    C:  copied        (not in HEAD,     in index, copied from another)
    T:  mode changed  (    in HEAD,     in index, same content       )
    U:  unmerged; conflict resolution required

The second column explains how the working directory differs from the index. Its values are always displayed in lower case and have the following meanings::

    -:  new / unknown (not in index,     in work tree                )
    m:  modified      (    in index,     in work tree, modified      )
    d:  deleted       (    in index, not in work tree                )

Sync
----
Updates the working tree to the latest revision.

The ``andromeda sync`` command synchronizes local project directories with the remote repositories specified in the manifest. If a local project does not yet exist, it will clone a new local directory from the remote repository and set up tracking branches as specified in the manifest. If the local project already exists, 'andromeda sync' will update the remote branches and rebase any new local changes on top of the new remote changes.

``andromeda sync`` will synchronize all projects listed at the command line. Projects can be specified either by name, or by a relative or absolute path to the project's local directory. If no projects are specified, ``andromeda sync`` will synchronize all projects listed in the manifest.

The ``-d``/``--detach`` option can be used to switch specified projects back to the manifest revision. This option is especially helpful if the project is currently on a topic branch, but the manifest revision is temporarily needed.

The ``-s``/``--smart-sync`` option can be used to sync to a known good build as specified by the manifest-server element in the current manifest. The ``-t``/``--smart-tag`` option is similar and allows you to specify a custom tag/label.

The ``-u``/``--manifest-server-username`` and ``-p``/``--manifest-server-password`` options can be used to specify a username and password to authenticate with the manifest server when using the ``-s`` or ``-t`` option.

If ``-u`` and ``-p`` are not specified when using the ``-s`` or ``-t`` option, ``andromeda sync`` will attempt to read authentication credentials for the manifest server from the user's ``.netrc`` file.

``andromeda sync`` will not use authentication credentials from ``-u``/``-p`` or ``.netrc`` if the manifest server specified in the manifest file already includes credentials.

The ``-f``/``--force-broken`` option can be used to proceed with syncing other projects if a project sync fails.

The ``--no-clone-bundle`` option disables any attempt to use ``$URL/clone.bundle`` to bootstrap a new Git repository from a resumeable bundle file on a content delivery network. This may be necessary if there are problems with the local Python HTTP client or proxy configuration, but the Git binary works.

The ``--fetch-submodules`` option enables fetching Git submodules of a project from server.

The ``-c``/``--current-branch`` option can be used to only fetch objects that are on the branch specified by a project's revision.

The ``--optimized-fetch`` option can be used to only fetch projects that are fixed to a sha1 revision if the sha1 revision does not already exist locally.

SSH Connections
^^^^^^^^^^^^^^^
If at least one project remote URL uses an SSH connection (``ssh://``, ``git+ssh://``, or ``user@host:path`` syntax) andromeda will automatically enable the SSH ControlMaster option when connecting to that host. This feature permits other projects in the same ``andromeda sync`` session to reuse the same SSH tunnel, saving connection setup overheads.

To disable this behavior on UNIX platforms, set the ``GIT_SSH`` environment variable to ``ssh``. For example::

    export GIT_SSH=ssh
    andromeda sync

Compatibility
~~~~~~~~~~~~~
This feature is automatically disabled on Windows, due to the lack of UNIX domain socket support.

This feature is not compatible with url.insteadof rewrites in the user's ``~/.gitconfig``. ``andromeda sync`` is currently not able to perform the rewrite early enough to establish the ControlMaster tunnel.

If the remote SSH daemon is Gerrit Code Review, version 2.0.10 or later is required to fix a server side protocol bug.

Upload
------
Upload changes for code review.

The ``andromeda upload`` command is used to send changes to the OpenStack Review system. It searches for topic branches in local projects that have not yet been published for review. If multiple topic branches are found, ``andromeda upload`` opens an editor to allow you to select which branches to upload.

``andromeda upload`` searches for uploadable changes in all projects listed at the command line. Projects can be specified either by name, or by a relative or absolute path to the project's local directory. If no projects are specified, ``andromeda upload`` will search for uploadable changes in all projects listed in the manifest.

If the ``--reviewers`` or ``--cc`` options are passed, those emails are added to the respective list of users, and emails are sent to any new users. Users passed as ``--reviewers`` must already be registered with the OpenStack Review system, or the upload will fail.

OpenStack Review URL
^^^^^^^^^^^^^^^^^^^^
https://review.openstack.org/

Configuration
^^^^^^^^^^^^^
These are read from each ``.git/config`` file the OpenStack repositories by default, but can be changed globally. To change this globally,

``review.URL.autoupload``:

To disable the ``Upload ... (y/N)?`` prompt, you can set a per-project or global Git configuration option. If review.URL.autoupload is set to ``true`` then repo will assume you always answer ``y`` at the prompt, and will not prompt you further. If it is set to ``false`` then andromeda will assume you always answer ``n``, and will abort.

``review.URL.autoreviewer``:

To automatically append a user or mailing list to reviews, you can set a per-project or global Git option to do so.

``review.URL.autocopy``:

To automatically copy a user or mailing list to all uploaded reviews, you can set a per-project or global Git option to do so. Specifically, ``review.URL.autocopy`` can be set to a comma separated list of reviewers who you always want copied on all uploads with a non-empty ``--re`` argument.

``review.URL.uploadtopic``:

To add a topic branch whenever uploading a commit, you can set a per-project or global Git option to do so. If review.URL.uploadtopic is set to ``true`` then andromeda will assume you always want the equivalent of the ``-t`` option to the andromeda command. If unset or set to ``false`` then andromeda will make use of only the command line option.