.. Andromeda documentation master file, created by
    sphinx-quickstart on Wed Jul 22 13:15:50 2015.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Andromeda Documentation
=======================

Andromeda is a Python-based repository management tool for the OpenStack repositories. This is a refactor of the `repo <https://android.googlesource.com/tools/repo/>`_ project for Android, but with some differences. While it is mostly identical for most intents and purposes, there are some differences:

* Works in Python 2.6-2.7, migration to Python 3.x will be complete soon.
* More PEP 8 compliant.
* It is configured for the OpenStack Gerrit instance, not Google's.
* Andromeda does not assume Gerrit user name is the first part of your email address...it must be set in the Git global configuration.
* This is an actual package instead of running ``curl http://... > file``.
* Better documentation.


Contents
^^^^^^^^

.. toctree::
    :maxdepth: 3

    quickstart
    commands
    manifest_xml
