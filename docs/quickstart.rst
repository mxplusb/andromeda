.. _quickstart:
.. highlight:: python

Quickstart
==========

For the impatient or those that want to figure it out independently, here is the
quick start variant with the OpenStack Sandbox repository::

    git config --global gitreview.username yourgerritusername
    andromeda init -u https://github.com/mynameismevin/andromeda-sandbox --config-name
    andromeda sync -j8
    andromeda start test-branch-01 sandbox
    cd sandbox && touch "test-file-01" && echo "Andromeda is awesome!" > test-file-01 && git add . && git commit -m "Andromeda still rocks!" && cd .."
    andromeda upload

Quickstart Explained
--------------------
Here is a quick and dirty explanation of what just happened.

1. Your Gerrit user name was configured globally for OpenStack.
2. Andromeda was initialised in the current directory and verified your information.
3. Andromeda pulled the remote repositories locally with 8 threads.
4. Andromeda initialised a new branch, ``test-branch-01``, in the ``sandbox`` repository.
5. Added a new file to the sandbox and committed it.
6. Created a review in the OpenStack Review board.

For more information on specific commands, please read the rest of the documentation. If you've ever used ``repo`` by Android, then you'll be fine, just replace ``repo``
with ``andromeda``.

